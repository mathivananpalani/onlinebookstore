<?php

class Publisher
{
	private $publisherId;
	private $country;
	
	public function setPublisherId(
				$publisherId
					) {
			$this->publisherId = $publisherId;
		}
	public function setCountry(
				$country
					) {
			$this->country = $country;
		}
	
	public function getPublisherid() : int {
			return $this->publisherId;
		}

	public function getCountry() : string {
			return $this->country;
		}
}