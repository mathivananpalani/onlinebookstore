<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
</head>
<body>
<div>
	<nav>
		<a href="header.php">Home</a>
		<a href="cart.php">Cart</a>
		<a href="order.php">Order</a>
	<?php if (!isset($_SESSION['username'])): ?>
		<a href="login.php">Login</a>
	<?php else: ?>
		<a href="logout.php">Logout</a>
	<?php endif; ?>
	</nav>
</div>