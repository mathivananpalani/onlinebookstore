<?php 
session_start();

require_once "Controller.php";
require_once "Model.php";
require_once "Database.php";

$model = new Model($pdo);
$controller = new Controller($model);

$controller -> index();
