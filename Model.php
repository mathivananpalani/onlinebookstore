<?php
spl_autoload_register(function($class){
	$path="$class.php";
	require_once $path;
});
class Model {

	private $db;
	private $temp;

	public function __construct($db) {
		$this->db = $db;
	}

	public function getBookDetails() {
		$sql ="select 
				book_id,
				title,
				authorName,
				genre,
				publicationyear,
				price,
				description,
				authorName,
				bookDetails.publisherId as publisherId,
				bookDetails.authorId as authorId,
				country
			from 
				bookDetails
				inner join author on bookDetails.authorId=author.authorId
				inner join publisher on bookDetails.publisherId = publisher.publisherId;";
		$query = $this->db-> query($sql);
		return $query;
		}
		
	}