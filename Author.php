<?php
class Author {
	private authorId;
	private authorName;
	
	public function setAuthorId(
				$authorId
				) {
			$this->authorId = $authorId;
		}
	public function setAuthorName(
				$authorName				
				) {
			$this->authorName = $authorName;
		}
	public function getAuthorId() : int {
			return $this->authorId;
		}

	public function getAuthorName() : string {
			return $this->authorName;
		}
}