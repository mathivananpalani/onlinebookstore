<?php 
require_once "header.php";
?>
<div>
<table>	
	<tr>
		<th> Bookname </th>
		<th> Author Name </th>
		<th> Published year </th>
		<th> Genre </th>
		<th> Price </th>
	</tr>
	<?php while ($row = $query->fetch()): ?>
            <tr>
                <td><a href="getbookdetails.php?title=<?php echo $row['title']; ?>">
                	<?php echo $row['title']; ?>
                	</a>
                </td>
                <td>
                	<?php echo $row['authorName']; ?>
                </td>
                <td>
                	<?php echo $row['publicationyear']; ?>
                </td>
				<td><?php echo $row['genre']; ?></td>
				<td><?php echo $row['price']; ?></td>
            </tr>
    <?php endwhile; ?>
</table>
</div>
</body>
</html>