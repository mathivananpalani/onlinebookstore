<?php
class User
{
	private $username;
	private $password;
	private $role;
	private $email;
	private $contactNumber;

	public function setUsername(
				$username
				) {
			$this -> username = $username;
		}
	
	public function setPassword(
			$password
				) {
			$this -> password = $password;
		}
	
	public function setRole(
		$role
		) {
			$this -> role = $role;
		}
	public function setEmail(
			$email
		) {
			$this -> email = $email;
		}
	public function setContactNumber(
			$email
		) {
			$this -> email = $email;
		}
	
	public function getUsername() : string {
			return $this->username;
		}

	public function getPassword() : string {
			return $this->password;
		}

	public function getRole() : string {
			return $this->role;
		}

	public function getEmail() : string {
			return $this->email;
		}
	public function getContactNumber()) : string {
		return $this->contactNumber;
	}
}