<?php

class Book {
	private $bookId;
	private $authorName;
	private $country;
	private $title;
	private $genre;
	private $publicationYear;
	private $price;
	private $description;
	
	public function setBookId(
			$bookId	
			) {
		$this -> $bookId = $bookId;

	}
	public function setAuthorName(
			$authorName) {
		$this -> authorName = $authorName;
	}
	public function setcountry(
			$country
			) {
		$this -> country = $country;
	}
	public function setTitle (
			$title
			) {
		$this -> title = $title;
	}
	public function setGenre (
				$genre
				) {
		$this -> genre=$genre;
	}
	public function setPublicationYear(
					$publicationYear
					) {
		$this -> publicationYear = $publicationYear;
	}
	public function setPrice(
				$price
				) {
		$this -> price = $price;
	}
	public function setDescription(
				$description
				) {
		$this -> description = $description;
	}
	public function getBookId():int {
		return $this -> bookId;	
	}	

	public function getAuthorName():string {
		return $this -> authorName;	
	}	

	public function getPublisherid():int {
		return $this -> publisherId;	
	}	
	
	public function getTitle():string {
		return $this -> title;	
	}	

	public function getGenre():string {
		return $this -> genre;	
	}	

	public function getPublicationyear():int {
		return $this -> publicationYear;	
	}

	public function getDescription():int {
		return $this -> description;	
	}
	
	public function getPrice():float {
		return $this -> price;	
	}	
}